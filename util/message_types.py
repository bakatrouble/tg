from telebot import AsyncTeleBot, util, TeleBot


class MessageType:
    def send(self, bot: AsyncTeleBot):
        raise NotImplementedError()


class TextMessage(MessageType):
    def __init__(self, text, chat_id=None, disable_web_page_preview=None, reply_to_message_id=None, reply_markup=None,
                 parse_mode=None, disable_notification=None):
        self.chat_id = chat_id
        self.text = text
        self.disable_web_page_preview = disable_web_page_preview
        self.reply_to_message_id = reply_to_message_id
        self.reply_markup = reply_markup
        self.parse_mode = parse_mode
        self.disable_notification = disable_notification

    def send(self, bot: AsyncTeleBot):
        for text in util.split_string(self.text, 4000):
            bot.send_message(self.chat_id, text, self.disable_web_page_preview, self.reply_to_message_id,
                             self.reply_markup, self.parse_mode, self.disable_notification)


class PhotoMessage(MessageType):
    def __init__(self, photo, chat_id=None, caption=None, reply_to_message_id=None, reply_markup=None,
                 disable_notification=None):
        self.chat_id = chat_id
        self.photo = photo
        self.caption = caption
        self.reply_to_message_id = reply_to_message_id
        self.reply_markup = reply_markup
        self.disable_notification = disable_notification

    def send(self, bot: AsyncTeleBot):
        caption = self.caption if len(self.caption) <= 200 else None
        bot.send_photo(self.chat_id, self.photo, caption, self.reply_to_message_id, self.reply_markup,
                       self.disable_notification)
        if not caption:
            for text in util.split_string(self.caption, 4000):
                bot.send_message(self.chat_id, text, None, None, self.reply_markup, None, self.disable_notification)


class AudioMessage(MessageType):
    def __init__(self, audio, chat_id=None, caption=None, duration=None, performer=None, title=None,
                 reply_to_message_id=None, reply_markup=None, parse_mode=None, disable_notification=None, timeout=None):
        self.chat_id = chat_id
        self.audio = audio
        self.caption = caption
        self.duration = duration
        self.performer = performer
        self.title = title
        self.reply_to_message_id = reply_to_message_id
        self.reply_markup = reply_markup
        self.parse_mode = parse_mode
        self.disable_notification = disable_notification
        self.timeout = timeout

    def send(self, bot: TeleBot):
        caption = self.caption if len(self.caption) <= 200 else None
        bot.send_audio(self.chat_id, self.audio, caption, self.duration, self.performer, self.title,
                       self.reply_to_message_id, self.reply_markup, self.parse_mode, self.disable_notification,
                       self.timeout)
        if not caption:
            for text in util.split_string(self.caption, 4000):
                bot.send_message(self.chat_id, text, None, None, self.reply_markup, self.parse_mode,
                                 self.disable_notification)


class VideoMessage(MessageType):
    pass


class VoiceMessage(MessageType):
    pass


class DocumentMessage(MessageType):
    pass


class ContactMessage(MessageType):
    pass


class LocationMessage(MessageType):
    pass


class StickerMessage(MessageType):
    pass


class GameMessage(MessageType):
    pass


class InvoiceMessage(MessageType):
    pass


class VenueMessage(MessageType):
    pass


class VideoNoteMessage(MessageType):
    pass
