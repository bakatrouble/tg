import importlib

from telebot import AsyncTeleBot
from telebot.types import Message

from util.message_types import TextMessage
import conf


def notify_admin(bot: AsyncTeleBot, message):
    TextMessage(message, conf.admin_chat_id, parse_mode='markdown').send(bot)


def get_plugin(name):
    config = conf.config.get(name, {})
    return importlib.import_module(f'plugins.{name}'), config


def is_chat_permitted(m: Message):
    return m.chat.type == 'private' and m.chat.id in conf.global_config['permitted_chats']
