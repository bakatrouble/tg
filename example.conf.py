from uuid import UUID

sentry_dsn = 'https://4dae81a8e40344789af1dbdc327baf91:2e0ebbf00e694baf8637d5f654292aea@example.com/0'

domain = 'https://example.com:443'

secret = UUID('aaaaaaaa-aaaa-aaaa-aaaa-aaaaaaaaaaaa')

admin_chat_id = -1

plugins = {
    'echo': '123456789:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
    'feed': '123456789:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
    'robot': '123456789:AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA',
}

config = {
    'feed': {
        'modules': [
            {'module': 'wp_comic', 'options': {'chat_id': -1000000000000, 'domain': 'poorlydrawnlines.com'}},
            {'module': 'wp_comic', 'options': {'chat_id': -1000000000000, 'domain': 'oppressive-silence.com'}},
            {'module': 'shittywatercolour', 'options': {'chat_id': -1000000000000}},
            {'module': 'tumblr_text', 'options': {'chat_id': -1000000000000, 'blog_id': 'aaa.tumblr.com'}},
            {'module': 'vk_wall', 'options': {'chat_id': -1000000000000, 'owner_id': -1, 'send_link': True}},
            {'module': 'navalnylive', 'options': {'chat_id': -1000000000000}},
        ],
    },
    'robot': {
        'upload_path': '/tmp/telebot',
        'result_url': 'http://localhost/%s'
    },
}

global_config = {
    'tumblr_api_key': '',
    'vk_api_key': '',
    'permitted_chats': [admin_chat_id, ],
}
