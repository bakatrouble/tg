#!/usr/bin/env python3

import uuid
import argparse
import sys
from typing import Dict
from logging import warning
import threading

import telebot
from aiohttp import web
import asyncio
import raven

import conf
import util


client = raven.Client(conf.sentry_dsn)

routes = {}  # type: Dict[str, telebot.AsyncTeleBot]

app = web.Application()


# webhook handler
async def handle(req):
    try:
        bot_id = req.match_info.get('bot_id')
        if bot_id in routes:
            request_body_dict = await req.json()
            update = telebot.types.Update.de_json(request_body_dict)
            routes[bot_id].process_new_updates([update])
            return web.Response()
        else:
            return web.Response(status=403)
    except:
        client.captureException()

app.router.add_post('/bots/{bot_id}', handle)


def init_bots(process_num):
    for name, token in conf.plugins.items():
        bot_id = uuid.uuid5(conf.secret, name + token).hex
        mod, config = util.get_plugin(name)
        try:
            routes[bot_id] = mod.init(telebot.TeleBot(token), config, conf.global_config)
            if process_num == 1:
                routes[bot_id].remove_webhook()
                routes[bot_id].set_webhook(f'{conf.domain}/bots/{bot_id}')
                warning(f'Plugin "{name}" is registered to {conf.domain}/bots/{bot_id}')
                if hasattr(mod, 'init_cron'):
                    mod.init_cron(routes[bot_id], config, conf.global_config)
        except AttributeError:
            warning(f'Plugin "{name}" has no `init(bot)` method')


def debug_loop(loop):
    asyncio.set_event_loop(loop)
    loop.run_forever()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Telegram bot framework')
    parser.add_argument('--debug', type=str, help='plugin name for debug', metavar='PLUGIN')
    parser.add_argument('--process_num', type=int, help='worker process number', metavar='N')
    parser.add_argument('--path', type=str, help='socket path', metavar='SOCKET_PATH')
    parser.add_argument('--port', type=int, help='tcp socket port', metavar='TCP_PORT')
    args = parser.parse_args()
    if not ((args.debug is None) ^ (args.process_num is None)):  # both set or both unset
        parser.error('Either --debug or --process_num should be passed, not none or both')
    if args.debug:
        loop = asyncio.get_event_loop()
        debug = args.debug

        mod, config = util.get_plugin(debug)
        bot = mod.init(telebot.TeleBot(conf.plugins[debug]), config, conf.global_config)
        if hasattr(mod, 'init_cron'):
            t = threading.Thread(target=debug_loop, args=(loop,))
            mod.init_cron(bot, config, conf.global_config)
            t.start()

        warning(f'Plugin "{debug}" has started')
        bot.remove_webhook()
        bot.polling()

        # exiting
        loop.stop()
        sys.exit(0)
    else:
        init_bots(process_num=args.process_num)
        web.run_app(app, path=args.path, port=args.port)
