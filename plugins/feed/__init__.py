import traceback
import json

from redis import Redis
from telebot import AsyncTeleBot
import asyncio
import aiocron

from app import client
from .modules import wp_comic, shittywatercolour, tumblr_text, vk_wall, navalnylive


class State:
    redis = None


feed_modules = {
    'wp_comic': wp_comic,
    'shittywatercolour': shittywatercolour,
    'tumblr_text': tumblr_text,
    'vk_wall': vk_wall,
    'navalnylive': navalnylive,
}


def init(bot: AsyncTeleBot, conf, global_conf):
    State.redis = Redis()
    return bot


def init_cron(bot: AsyncTeleBot, conf, global_conf):
    @aiocron.crontab('*/10 * * * *')
    @asyncio.coroutine
    def do_work():
        for entry in conf['modules']:
            try:
                key = f"tg_feed-{entry['module']}-{entry['options']['chat_id']}"
                last_id = State.redis.get(key) or 0
                last_id, result = feed_modules[entry['module']](last_id, entry['options'], global_conf)
                State.redis.set(key, last_id)
                for item in result:
                    item.chat_id = entry['options']['chat_id']
                    try:
                        item.send(bot)
                    except:
                        client.captureException()
            except:
                client.captureException()
