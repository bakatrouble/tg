import requests

from util.message_types import PhotoMessage, AudioMessage


def navalnylive(last_created, options, global_conf):
    last_created = int(last_created)
    result = []
    posts = requests.get('https://radio.navalny.live/latest', headers={
        'User-agent': 'bakatrouble FeedBot 0.1',
        'Authorization': 'Token 021e668a9a94887b7f29b2ed5341513ccc193200',
    }).json()
    posts = posts['results']
    for p in sorted(posts, key=lambda x: x['id']):
        if p['id'] > last_created:
            title = f'{p["show__name"]} #{p["number"]}'
            result.append(PhotoMessage(requests.get(p['cover_url']).content, caption=p['youtube_url']))
            result.append(
                AudioMessage(requests.get(p['audio_url']).content, caption=f'{title}: {p["name"]}',
                             performer='Navalny Live', title=title)
            )
            last_created = p['id']
    return last_created, result
