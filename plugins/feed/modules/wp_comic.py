import requests
from util.message_types import PhotoMessage


def wp_comic(last_id, options, global_conf):
    last_id = int(last_id)
    result = []
    media: list = requests.get('http://%s/wp-json/wp/v2/media' % options['domain']).json()
    for m in filter(lambda x: x['id'] > last_id, reversed(media)):
        result.append(PhotoMessage(caption=m['title']['rendered'],
                                   photo=requests.get(m['source_url']).content))
        last_id = m['id']
    return last_id, result
