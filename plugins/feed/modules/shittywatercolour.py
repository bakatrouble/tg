import requests

from util.message_types import PhotoMessage


def shittywatercolour(last_created, options, global_conf):
    last_created = float(last_created)
    result = []
    posts = requests.get('https://www.reddit.com/user/Shitty_Watercolour.json',
                         headers={'User-agent': 'bakatrouble FeedBot 0.1'}).json()
    posts = posts['data']['children']
    for p in reversed(posts):
        if p['data']['created'] > last_created and\
                             p['kind'] == 't3' and\
                             'post_hint' in p['data'] and\
                             p['data']['post_hint'] == 'image':
            p = p['data']
            result.append(PhotoMessage(caption=p['title'],
                                       photo=requests.get(p['url']).content))
            last_created = p['created']
    return last_created, result
