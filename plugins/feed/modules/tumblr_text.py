import requests

from util.message_types import TextMessage


def tumblr_text(last_id, options, global_conf):
    result = []
    last_id = int(last_id)
    data = requests.get(f"https://api.tumblr.com/v2/blog/{options['blog_id']}/posts/text", {
        'api_key': global_conf['tumblr_api_key'],
        'filter': 'text',
    }).json()
    if data['meta']['msg'] != 'OK':
        raise ValueError(data['meta'])
    for post in reversed(data['response']['posts']):
        if post['id'] > last_id:
            result.append(TextMessage(f"*{post['title']}*\n\n{post['body']}\n\n{post['short_url']}",
                                      parse_mode='markdown'))
            last_id = post['id']
    return last_id, result
