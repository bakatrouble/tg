import requests
from util.message_types import TextMessage, PhotoMessage


def get_photo(attachment):
    for size in (2560, 1280, 807, 604, 130, 75):
        if f'photo_{size}' in attachment:
            return attachment[f'photo_{size}']
    return None


def vk_wall(last_id, options, global_conf):
    result = []
    last_id = int(last_id)
    data = requests.get('https://api.vk.com/method/wall.get', {
        'v': '5.67',
        'access_token': global_conf['vk_api_key'],
        'owner_id': options['owner_id'],
    }).json()
    if 'response' not in data:
        raise ValueError(data)
    for post in reversed(data['response']['items']):
        if post['id'] > last_id:
            result.append(TextMessage(post['text']))
            if 'attachments' in post:
                for image in filter(lambda a: a['type'] == 'photo', post['attachments']):
                    image = image['photo']
                    image_url = get_photo(image)
                    if image_url:
                        result.append(PhotoMessage(requests.get(image_url).content, caption=image['text']))
            if options.get('send_link', True):
                result.append(TextMessage(f"https://vk.com/wall{post['owner_id']}_{post['id']}",
                                          disable_web_page_preview=True))
            last_id = post['id']
    return last_id, result
