from telebot import AsyncTeleBot


def init(bot: AsyncTeleBot, conf, global_conf):
    @bot.message_handler(func=lambda x: True, content_types=['text'])
    def echo(msg):
        bot.reply_to(msg, msg.text)

    return bot
