import os
import random
import re

from telebot import AsyncTeleBot, TeleBot
from telebot.types import Message, File

from util import is_chat_permitted


SUIKA_ANSWERS = (
    'Су*ка',
    'Ты имел в виду Су*ку?',
    'Вот же Су*ка',
    'Су*ечка',
    lambda x: '*' + re.sub('й', '*', x, flags=re.I)
)


def init(bot: AsyncTeleBot, conf, global_conf):
    # noinspection PyCallByClass
    @bot.message_handler(func=is_chat_permitted, content_types=['photo', 'audio', 'video', 'document'])
    def save_file(msg: Message):
        if msg.content_type == 'photo':
            file_id = msg.photo[-1].file_id
        elif msg.content_type == 'audio':
            file_id = msg.audio.file_id
        elif msg.content_type == 'video':
            file_id = msg.video.file_id
        elif msg.content_type == 'document':
            file_id = msg.document.file_id
        else:
            return

        file = TeleBot.get_file(bot, file_id)  # type: File
        filename = os.path.basename(file.file_path)

        with open(os.path.join(conf['upload_path'], filename), 'wb') as f:
            f.write(TeleBot.download_file(bot, file.file_path))

        bot.reply_to(msg, conf['result_url'] % filename)

    @bot.message_handler(regexp=r'(суйк\w+)')
    def suika_echo(msg: Message):
        text = msg.text
        if text:
            match = re.search(r'(суйк\w+)', text, flags=re.I)
            if match:
                reply = random.choice(SUIKA_ANSWERS)
                if type(reply) != str:
                    reply = reply(match.group(1))
                bot.reply_to(msg, reply)

    @bot.message_handler(commands=['roll'])
    def roll(msg: Message):
        argv = msg.text.split(' ')
        start = 0
        end = 100

        try:
            if len(argv) == 2:
                end = int(argv[1])
            elif len(argv) == 3:
                start = int(argv[1])
                end = int(argv[2])

            if end < start:
                raise ValueError
        except ValueError:
            return

        bot.reply_to(msg, str(random.randint(start, end)))

    return bot
