import tempfile

from telebot import TeleBot
from telebot.apihelper import ApiException
from telebot.types import Message, ForceReply
from PIL import Image

from conf import admin_chat_id


def init(bot: TeleBot, conf, global_conf):
    @bot.message_handler(func=lambda x: True, content_types=['sticker', 'document'])
    def sticker(msg: Message):
        if msg.chat.id != admin_chat_id:
            return bot.reply_to(msg, 'Go away')

        if msg.content_type == 'sticker':
            file_info = bot.get_file(msg.sticker.file_id)
            file_content = bot.download_file(file_info.file_path)
            with tempfile.NamedTemporaryFile(suffix='.webp') as f:
                f.write(file_content)
                f.seek(0)
                im = Image.open(f).convert('RGBA')
            with tempfile.NamedTemporaryFile(suffix='.png') as f:
                im.save(f, 'png')
                f.seek(0)
                try:
                    bot.add_sticker_to_set(admin_chat_id, 'pack_by_bakatrouble_stickerbot', f, msg.sticker.emoji)
                    bot.reply_to(msg, 'Done!')
                except ApiException as e:
                    bot.reply_to(msg, f'Error: ```\n{e}\n```', parse_mode='markdown')
        else:
            bot.register_next_step_handler(msg, set_emoji)
            bot.send_document(msg.chat.id, msg.document.file_id, caption='Please input emojis for sticker',
                              reply_markup=ForceReply())

    def set_emoji(msg: Message):
        try:
            bot.add_sticker_to_set(admin_chat_id, 'pack_by_bakatrouble_stickerbot',
                                   msg.reply_to_message.document.file_id, msg.text)
            bot.reply_to(msg, 'Done!')
        except ApiException as e:
            bot.reply_to(msg, f'Error: ```\n{e}\n```', parse_mode='markdown')

    return bot
